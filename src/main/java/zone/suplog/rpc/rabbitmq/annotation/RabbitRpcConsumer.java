package zone.suplog.rpc.rabbitmq.annotation;

import java.lang.annotation.*;

@Target({ElementType.CONSTRUCTOR, ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RabbitRpcConsumer {

    String applicationName() default "";

    String exchange() default "";

    long maxConnTimeout() default 5000L;

}
