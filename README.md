<h1 align="center">SuplogRpc</h1>
<p align="center">
	<strong>轻配置 敏捷部署 代码无侵入 自定义扩展</strong>
</p>
<p align="center">
	<a target="_blank" href="https://spring.io/projects/spring-boot">
		<img src="https://img.shields.io/badge/spring%20boot-2.5.0-yellowgreen" />
	</a>
    <a target="_blank" href="https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html">
		<img src="https://img.shields.io/badge/JDK-8+-green.svg" />
	</a>
	<a target="_blank" href="https://www.apache.org/licenses/LICENSE-2.0.html">
		<img src="https://img.shields.io/badge/license-Apache--2.0-blue" />
	</a>
</p>

#### 介绍

SuplogRpc是借力于RabbitMQ、RocketMQ、Kafka等市面上常用中间件的强大通信能力而生的低配置、轻量级rpc框架，性能上取决于mq产品在性能瓶颈方面的优化，功能上依附于mq产品的负载均衡、消息幂等、消息事务等特性。
依附mq好处是其自身的功能特性可以满足rpc框架在使用过程中需要的大部分的基础功能。

另外，每家公司的技术选型都会因为业务的特殊性而有差别，所列的rabbitmq、rocketmq、kafka等可能不是所有项目都采用。
所以suplog-rpc顺便提供了丰富的spi接口，开发者在使用该框架的过程中，可基于spi规范自由定制rpc通信框架，实现适配自己项目架构的rpc。

有各种问题欢迎随时拍砖，热烈欢迎有兴趣的小伙伴参与贡献~

#### 为什么要写这个框架

我喜欢轻便、简洁、实用和自由的代码编辑模式。

在轻便、简洁、实用方面，SpringCloud、Dubbo在相反的方向渐行渐远，大部分功能我们可能都不会用到， 但带来的配置繁琐，部署、学习和维护成本高都是必须淌过的大江大河。

换句话说，这些含着金汤匙的rpc框架，它们各自孵化于全球排名前列的互联网公司，拥有天然的高并发、大数据场景， 必须要考虑CAP，性能瓶颈，可插拔，大量灵活的配置等等，方方面面都必须考虑全面，不可能再做到轻和简。

它们是为大公司而生的，并不适合并发量不高，但业务迭代快，需要快速部署、可扩展、边界清晰的业务系统设计的中小型公司。

本框架使用常用消息中间件作rpc通信层，开箱即用，微服务化，低配置，快速部署，代码无侵入性，并沿用了Seata提供的分布式事务解决方案。

用Seata这方面，因为事务也是日常必须用到的。 把顺便用到的消息中间件、事务调整成一个适合生产的RPC，也是顺水推舟的体现。 把刚需搭配精尽其用，我认为是借力打力,一物多用,不重复造轮子。这也是我对轻便、简洁和实用的理解。

不用再额外考虑rpc的种种诸如： 服务副本是不是一致？脑裂了怎么办？主从选举该用什么算法？rpc路由该怎么分配权重？请求链路是不是幂等？ 把这些问题交给mq中间件就行了，事实上它们都已经解决了。

本项目计划将支持rabbitmq、rocketmq、kafka。 所有的维护难题都转移至消息中间件层面，不再为繁重的rpc注册中心、多如牛毛的组件配置而烦恼。 配置简单，天然适配spring开发环境，部署即用，没有额外的注册中心之类的。

在自由方面，在实现功能的同时，也抽象出了丰富的spi接口。开发者在了解了框架的运行原理后， 可根据自身情况，自定义rpc的通信和路由，在spi接口的规范下，开发一款适合自己的rpc框架。

#### 开发计划

- 适配RabbitMQ `已支持`
- 适配RocketMQ `开发中`
- 适配KafkaMQ`规划中`
- 适配Http`规划中`
- 适配Netty`规划中`

#### 示例下载

> 消费端:
> rpc-client(服务调用方)

> 服务端:
> rpc-service-impl(业务包)、rpc-service(暴露接口包)

```markdown
git clone https://gitee.com/suplog-rpc/suplog-spring-boot-starter-rpc-example.git
```

#### 快速启动

##### 基于Rabbit-MQ

###### 下载仓库
    - spi基础包(属于suplog-spring-boot-starter-rpc-rabbitmq的依赖包)
   ```markdown
   git clone https://gitee.com/suplog-rpc/suplog-rpc-spi.git
   ```
    - 适配rabbitmq模式的suplog-rpc
   ```markdown
    git clone https://gitee.com/suplog-rpc/suplog-spring-boot-starter-rpc-rabbitmq.git
   ```
###### 部署本地仓库(以上两个包按先后顺序install)

   ```markdown
    mvn clean install
   ```

###### 部署消费端(rpc-client)

    1. 引入dependency
   ```markdown
       <-- RPC服务 -->
       <dependency>
           <groupId>zone.suplog</groupId>
           <artifactId>suplog-spring-boot-starter-rpc-rabbitmq</artifactId>
           <version>0.0.1</version>
       </dependency>
       <-- 某业务生产端服务接口 -->
       <dependency>
            <groupId>com.example</groupId>
            <artifactId>rpc-service</artifactId>
            <version>0.0.1-SNAPSHOT</version>
       </dependency>
   ```
    2. application.yml配置
   ```yaml
   spring:
       application:
         name: rpc-client
       rabbitmq:
         host: localhost
         port: 5672
         username: root
         password: 123456
       suplog-rpc:
         rabbit-mq:
           # 服务消费配置
           consumer:
               # 消费服务所用交换机需要和服务注册为同一交换机
               exchange: exchange-rpc
               # 最大连接超时时间，默认5000毫秒
               max-conn-timeout: 3000
   ```

    3. 消费端处理器配置
    ```java
      @Configuration
      public class SuplogRpcConfig {
   
      /**
      * 消费端处理器配置
      */
       @Bean
       public RabbitRpcConsumerProcessor rabbitMQRpcConsumerProcessor() {
           RabbitRpcConsumerProcessor rabbitRpcConsumerProcessor = new RabbitRpcConsumerProcessor();
           return rabbitRpcConsumerProcessor;
       }
   }

   ```
    4. 注解方式注入客户端协议接口并使用
    ```java
      @RestController
      @RequestMapping("/demo")
      public class DemoController {
      
          @RabbitRpcConsumer
          private DemoService demoService;
      
          @GetMapping({"/print"})
          public String print(String str) {
              return demoService.print(str);
          }
      
          @GetMapping("/println")
          public void println(String str) {
              demoService.println(str);
          }
      }
   ```

    5. 启动工程


###### 部署服务/生产端(rpc-service-impl)

    1. 引入dependency
   ```markdown
       <-- RPC服务 -->
       <dependency>
           <groupId>zone.suplog</groupId>
           <artifactId>suplog-spring-boot-starter-rpc-rabbitmq</artifactId>
           <version>0.0.1</version>
       </dependency>
       <-- 本服务对外暴露接口 -->
       <dependency>
            <groupId>com.example</groupId>
            <artifactId>rpc-service</artifactId>
            <version>0.0.1-SNAPSHOT</version>
       </dependency>
   ```

    2. application.yml配置
   ```yaml
   spring:
      application:
        name: rpc-service-impl
      rabbitmq:
        host: localhost
        port: 5672
        username: root
        password: 123456
      suplog-rpc:
        rabbit-mq:
          # 服务暴露配置
          provider:
            # 交换机名，负责服务寻找
            exchange: exchange-rpc
            # 服务名，也是队列名
            application-name: ${spring.application.name}
            # rpc最大连接超时时间，默认5000毫秒
            max-conn-timeout: 3000
            # 路由匹配服务根路径键注册，接口根包名和服务所在根包名需一致
            registry-key: com.example.rpcservice.service
            # 以下为接收请求处理所使用线程池配置
            # 核心线程数量，默认8个
            core-pool-size: 8
            # 线程池最大线程数量，默认200个
            max-pool-size: 200
            # 线程池最大线程数量，默认200个
            queue-capacity: 200
            # 超过core-size的那些线程，任务完成后，再经过这个时长（秒）会被结束掉。默认120秒
            keep-alive-seconds: 120
      
      logging.level.zone.suplog.rpc: DEBUG
   ```
    3. 服务端处理器配置
   ```java
      @Configuration
      public class SupRpcConfig {
      
          @Bean
          public RabbitRpcProviderProcessor rpcProviderRabbitMQProcessor() {
              return new RabbitRpcProviderProcessor();
          }
      }
    
   ```

   4.启动

> springBoot常规启动

###### 注意事项

- 配置`spring.suplog.rabbit-mq.consumer.exchange`交换机名称需要和`spring.suplog-rpc.rabbit-mq.provider.exchange`
  一致，否则会找不到服务。你可以理解为exchange是一个注册中心，如果消费端不在同一个注册中心寻找服务，肯定是无法找到的。
- 在创建协议接口包时，接口包根包名需要和服务根包名一致。如以上事例中：`rpc-service-impl`的根包名为`com.example.rpcservice`
  ，则配置`spring.suplog-rpc.rabbit-mq.provider.registry-key`为`com.example.rpcservice`，
  `rpc-service`接口包根包名也必须是`com.example.rpcservice`。
  原因是rabbit用做rpc通信层，基于TOPIC模式进行路由寻址服务，routingKey为`com.example.rpcservice.#`

##### 基于Rocket-MQ

> 集成中

##### 基于Kafka-MQ

> 规划中

##### 基于Http

> 规划中

##### 基于Netty

> 规划中

#### 关于SPI接口

开发者如需定制适合自己项目架构的rpc服务，例如自家正在用ActivityMQ,那么可以参见`https://gitee.com/suplog-rpc/suplog-rpc-spi.git`包抽象接口，并参考`https://gitee.com/suplog-rpc/suplog-spring-boot-starter-rpc-rabbitmq.git`源码的子类实现。

#### 分布式事务

集成中